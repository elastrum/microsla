package hu.microsec.microsla.image

import hu.microsec.microsla.image.domain.BaseImage
import hu.microsec.microsla.image.domain.Image
import hu.microsec.microsla.image.repository.BaseImageRepository
import hu.microsec.microsla.image.repository.ImageRepository
import hu.microsec.microsla.image.service.ImageService
import io.kotest.core.spec.style.StringSpec
import io.micronaut.test.annotation.MicronautTest
import java.io.FileOutputStream

@MicronautTest
class MicroslaImageTest(
        private val imageService: ImageService,
        private val baseImageRepository: BaseImageRepository,
        private val imageRepository: ImageRepository
) : StringSpec({

    "merge images" {
        baseImageRepository.saveAndFlush(BaseImage("a.png"))

        imageRepository.saveAll(listOf(
                Image("b", "b.png", 1),
                Image("c", "c.png", 2)
        ))
        imageRepository.flush()

        val image = imageService.getImage(listOf("b", "c"))

        FileOutputStream("build/mergedImage.png").use {
            it.write(image)
        }
    }
})
