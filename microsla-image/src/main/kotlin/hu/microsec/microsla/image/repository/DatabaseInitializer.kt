package hu.microsec.microsla.image.repository

import hu.microsec.microsla.image.domain.BaseImage
import hu.microsec.microsla.image.domain.Image
import io.micronaut.discovery.event.ServiceReadyEvent
import io.micronaut.runtime.event.annotation.EventListener
import io.micronaut.scheduling.annotation.Async
import javax.inject.Singleton
import javax.transaction.Transactional

@Singleton
class DatabaseInitializer(
        private val baseImageRepository: BaseImageRepository,
        private val imageRepository: ImageRepository
) {
    @Async
    @EventListener
    @Transactional
    fun initializeDatabase(serviceReadyEvent: ServiceReadyEvent) {
        if (baseImageRepository.count() == 0L) {
            baseImageRepository.save(BaseImage("microsla.png"))
        }

        if (imageRepository.count() == 0L) {
            imageRepository.saveAll(listOf(
                    Image("paint", "paint.png", 1),
                    Image("rear_wing", "rear_wing.png", 2),
                    Image("eszigno_sticker", "logo.png", 3),
                    Image("v2x", "v2x.png", 4)
            ))
        }
    }
}
