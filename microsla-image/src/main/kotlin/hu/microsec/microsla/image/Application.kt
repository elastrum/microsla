package hu.microsec.microsla.image

import io.micronaut.runtime.Micronaut.*
import io.swagger.v3.oas.annotations.*
import io.swagger.v3.oas.annotations.info.*

@OpenAPIDefinition(
    info = Info(
            title = "microsla-image",
            version = "0.0"
    )
)
object Api

fun main(args: Array<String>) {
	build()
	    .args(*args)
		.packages("hu.microsec.microsla.image")
		.start()
}

