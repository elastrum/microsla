package hu.microsec.microsla.image.repository

import hu.microsec.microsla.image.domain.BaseImage
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository

@Repository
interface BaseImageRepository : JpaRepository<BaseImage, Long>
