package hu.microsec.microsla.image.domain

import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Image(@Id val id: String, val filePath: String, val priority: Int)
