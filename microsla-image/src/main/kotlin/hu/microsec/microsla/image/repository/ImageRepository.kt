package hu.microsec.microsla.image.repository

import hu.microsec.microsla.image.domain.Image
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository

@Repository
interface ImageRepository : JpaRepository<Image, String> {
    fun findByIdIn(ids: List<String>): List<Image>
}
