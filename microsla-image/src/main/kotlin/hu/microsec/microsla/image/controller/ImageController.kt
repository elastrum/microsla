package hu.microsec.microsla.image.controller

import hu.microsec.microsla.common.ImageOperations
import hu.microsec.microsla.image.service.ImageService
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.QueryValue

@Controller("/api/v1/image")
class ImageController(private val imageService: ImageService) : ImageOperations {

    @Get
    @Produces(MediaType.IMAGE_PNG)
    override fun getImage(@QueryValue extraIds: List<String>): ByteArray = imageService.getImage(extraIds)
}
