package hu.microsec.microsla.image.domain

import javax.persistence.Entity
import javax.persistence.Id

const val BASE_IMAGE_SINGLETON_ID: Long = 0L

@Entity
data class BaseImage(val filePath: String) {
    @Id val id: Long = BASE_IMAGE_SINGLETON_ID
}
