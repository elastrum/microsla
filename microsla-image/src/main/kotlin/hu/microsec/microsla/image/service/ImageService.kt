package hu.microsec.microsla.image.service

import hu.microsec.microsla.image.domain.BASE_IMAGE_SINGLETON_ID
import hu.microsec.microsla.image.domain.Image
import hu.microsec.microsla.image.repository.BaseImageRepository
import hu.microsec.microsla.image.repository.ImageRepository
import io.micronaut.context.annotation.Property
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.nio.file.Paths
import java.util.Collections.max
import javax.imageio.ImageIO
import javax.inject.Singleton

@Singleton
class ImageService(
        private val imageRepository: ImageRepository,
        private val baseImageRepository: BaseImageRepository,
        @Property(name = "imageStore.baseDir") private val baseDir: String
) {

    fun getImage(extraIds: List<String>): ByteArray {
        val baseImagePath = baseImageRepository
                .findById(BASE_IMAGE_SINGLETON_ID)
                .orElseThrow { IllegalStateException("Base image missing") }
                .filePath
        val extraImagePaths = imageRepository
                .findByIdIn(extraIds)
                .sortedBy(Image::priority)
                .map(Image::filePath)
        val allImagesSortedByPriority = (listOf(baseImagePath) + extraImagePaths).mapNotNull {
            try {
                it.asImage()
            } catch (e: IOException) {
                null
            }
        }
        return composeImage(allImagesSortedByPriority)
    }

    private fun String.asImage(): BufferedImage {
        val path = Paths.get(baseDir, this)
        return if (path.isAbsolute)
            ImageIO.read(path.toFile())
        else ImageService::class.java.classLoader.getResourceAsStream(path.toString())?.let { ImageIO.read(it) }
                ?: throw FileNotFoundException("File not found: $path")
    }

    private fun composeImage(images: List<BufferedImage>): ByteArray {
        val width = max(images.map(BufferedImage::getWidth))
        val height = max(images.map(BufferedImage::getHeight))
        val composite = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
        val graphics = composite.graphics
        images.forEach { graphics.drawImage(it, 0, 0, null) }
        graphics.dispose()
        val outputStream = ByteArrayOutputStream()
        ImageIO.write(composite, "PNG", outputStream)
        return outputStream.toByteArray()
    }
}

