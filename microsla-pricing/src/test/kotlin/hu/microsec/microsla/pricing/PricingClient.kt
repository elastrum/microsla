package hu.microsec.microsla.pricing

import hu.microsec.microsla.common.ExtraPrice
import hu.microsec.microsla.common.PricingOperations
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.annotation.Client

@Client("/api/v1/pricing")
interface PricingClient : PricingOperations {

    @Get("/base")
    override fun getBasePrice(): Int

    @Get("/extras")
    override fun getExtraPrices(): List<ExtraPrice>

    @Get("/config")
    override fun getPriceForConfiguration(@QueryValue extraIds: List<String>): Int
}
