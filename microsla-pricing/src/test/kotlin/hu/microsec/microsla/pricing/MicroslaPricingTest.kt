package hu.microsec.microsla.pricing

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.ints.shouldBeGreaterThan
import io.kotest.matchers.shouldBe
import io.micronaut.test.annotation.MicronautTest

@MicronautTest
class MicroslaPricingTest(private val pricingClient: PricingClient) : StringSpec({

    "get base price" {
        pricingClient.getBasePrice().shouldBeGreaterThan(0)
    }

    "get price for extras" {
        pricingClient.getExtraPrices().size.shouldBe(7)
    }

    "get price for configuration" {
        pricingClient.getPriceForConfiguration(listOf("v2x", "paint")).shouldBeGreaterThan(0)
    }
})
