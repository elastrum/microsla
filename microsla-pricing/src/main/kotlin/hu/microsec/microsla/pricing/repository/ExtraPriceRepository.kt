package hu.microsec.microsla.pricing.repository

import hu.microsec.microsla.pricing.domain.ExtraPrice
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository

@Repository
interface ExtraPriceRepository : JpaRepository<ExtraPrice, String>
