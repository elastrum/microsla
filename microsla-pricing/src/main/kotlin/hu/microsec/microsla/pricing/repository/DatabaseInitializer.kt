package hu.microsec.microsla.pricing.repository

import hu.microsec.microsla.pricing.domain.BASE_PRICE_SINGLETON_ID
import hu.microsec.microsla.pricing.domain.BasePrice
import hu.microsec.microsla.pricing.domain.ExtraPrice
import io.micronaut.discovery.event.ServiceReadyEvent
import io.micronaut.runtime.event.annotation.EventListener
import io.micronaut.scheduling.annotation.Async
import javax.inject.Singleton
import javax.transaction.Transactional

private const val INITIAL_BASE_PRICE = 38000

@Singleton
class DatabaseInitializer(
        private val basePriceRepository: BasePriceRepository,
        private val extraPriceRepository: ExtraPriceRepository
) {
    @Async
    @EventListener
    @Transactional
    fun initializeDatabase(serviceReadyEvent: ServiceReadyEvent) {
        if (!basePriceRepository.existsById(BASE_PRICE_SINGLETON_ID)) {
            basePriceRepository.save(BasePrice(BASE_PRICE_SINGLETON_ID, INITIAL_BASE_PRICE))
        }

        if (extraPriceRepository.count() == 0L) {
            extraPriceRepository.saveAll(listOf(
                    ExtraPrice("v2x", 480),
                    ExtraPrice("leather_seats", 800),
                    ExtraPrice("parking_assist", 220),
                    ExtraPrice("winter_package", 750),
                    ExtraPrice("paint", 500),
                    ExtraPrice("rear_wing", 300),
                    ExtraPrice("eszigno_sticker", 120)
            ))
        }
    }
}
