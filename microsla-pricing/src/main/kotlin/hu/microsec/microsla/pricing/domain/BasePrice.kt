package hu.microsec.microsla.pricing.domain

import javax.persistence.Entity
import javax.persistence.Id

const val BASE_PRICE_SINGLETON_ID: Long = 0L

@Entity
data class BasePrice(
        @Id val id: Long,
        val price: Int
)
