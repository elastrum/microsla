package hu.microsec.microsla.pricing.controller

import hu.microsec.microsla.common.ExtraPrice
import hu.microsec.microsla.common.PricingOperations
import hu.microsec.microsla.pricing.service.PricingService
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue

@Controller("/api/v1/pricing")
class PricingController(private val pricingService: PricingService) : PricingOperations {

    @Get("/base")
    override fun getBasePrice(): Int = pricingService.getBasePrice()

    @Get("/extras")
    override fun getExtraPrices(): List<ExtraPrice> = pricingService.getExtraPrices()

    @Get("/config")
    override fun getPriceForConfiguration(@QueryValue extraIds: List<String>): Int = pricingService.getPriceForConfiguration(extraIds)
}
