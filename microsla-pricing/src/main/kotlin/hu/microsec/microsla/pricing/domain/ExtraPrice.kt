package hu.microsec.microsla.pricing.domain

import hu.microsec.microsla.common.ExtraPrice
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class ExtraPrice(
        @Id override val id: String,
        override val price: Int
) : ExtraPrice(id, price)
