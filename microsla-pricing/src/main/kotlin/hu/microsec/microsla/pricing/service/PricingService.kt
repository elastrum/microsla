package hu.microsec.microsla.pricing.service

import hu.microsec.microsla.pricing.domain.BASE_PRICE_SINGLETON_ID
import hu.microsec.microsla.pricing.domain.BasePrice
import hu.microsec.microsla.pricing.domain.ExtraPrice
import hu.microsec.microsla.pricing.repository.BasePriceRepository
import hu.microsec.microsla.pricing.repository.ExtraPriceRepository
import javax.inject.Singleton

@Singleton
class PricingService(
        private val basePriceRepository: BasePriceRepository,
        private val extraPriceRepository: ExtraPriceRepository
) {

    fun getBasePrice(): Int = basePriceRepository.findById(BASE_PRICE_SINGLETON_ID)
            .map(BasePrice::price)
            .orElseThrow { IllegalStateException("Base price not found in database") }

    fun getPriceForConfiguration(extraIds: List<String>): Int = getBasePrice() + extraIds.map(::getPriceForExtra).sumBy {
        it ?: 0
    }

    private fun getPriceForExtra(extraId: String): Int? = extraPriceRepository
            .findById(extraId)
            .map(ExtraPrice::price)
            .orElse(null)

    fun getExtraPrices(): List<ExtraPrice> = extraPriceRepository.findAll()
}
