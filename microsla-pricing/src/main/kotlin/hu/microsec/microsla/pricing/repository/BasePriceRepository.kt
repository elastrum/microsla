package hu.microsec.microsla.pricing.repository

import hu.microsec.microsla.pricing.domain.BasePrice
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository

@Repository
interface BasePriceRepository : JpaRepository<BasePrice, Long>
