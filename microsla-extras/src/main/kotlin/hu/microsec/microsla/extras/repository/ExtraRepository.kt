package hu.microsec.microsla.extras.repository

import hu.microsec.microsla.extras.domain.Extra
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository

@Repository
interface ExtraRepository : JpaRepository<Extra, String>
