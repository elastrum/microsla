package hu.microsec.microsla.extras.repository

import hu.microsec.microsla.extras.domain.Extra
import io.micronaut.discovery.event.ServiceReadyEvent
import io.micronaut.runtime.event.annotation.EventListener
import io.micronaut.scheduling.annotation.Async
import javax.inject.Singleton
import javax.transaction.Transactional

@Singleton
class DatabaseInitializer(private val extraRepository: ExtraRepository) {
    @Async
    @EventListener
    @Transactional
    fun initializeDatabase(serviceReadyEvent: ServiceReadyEvent) {
        if (extraRepository.count() == 0L) {
            extraRepository.saveAll(listOf(
                    Extra("v2x", "Communication module powered by Microsec V2X PKI™"),
                    Extra("leather_seats", "Leather seats"),
                    Extra("parking_assist", "Parking assist"),
                    Extra("winter_package", "Winter package (heated seats and steering wheel)"),
                    Extra("paint", "Metallic paint (Microsec Yellow)"),
                    Extra("rear_wing", "Rear wing"),
                    Extra("eszigno_sticker", "E-szignó sticker")
            ))
        }
    }
}
