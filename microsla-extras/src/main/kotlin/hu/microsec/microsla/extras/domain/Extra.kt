package hu.microsec.microsla.extras.domain

import hu.microsec.microsla.common.Extra
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Extra(@Id override val id: String, override val description: String) : Extra(id, description)
