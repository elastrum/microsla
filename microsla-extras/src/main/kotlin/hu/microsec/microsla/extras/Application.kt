package hu.microsec.microsla.extras

import io.micronaut.runtime.Micronaut.*
import io.swagger.v3.oas.annotations.*
import io.swagger.v3.oas.annotations.info.*

@OpenAPIDefinition(
    info = Info(
            title = "microsla-extras",
            version = "0.0"
    )
)
object Api

fun main(args: Array<String>) {
	build()
	    .args(*args)
		.packages("hu.microsec.microsla.extras")
		.start()
}

