package hu.microsec.microsla.extras.controller

import hu.microsec.microsla.common.Extra
import hu.microsec.microsla.common.ExtraOperations
import hu.microsec.microsla.extras.service.ExtraService
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get

@Controller("/api/v1/extras")
class ExtraController(private val extraService: ExtraService) : ExtraOperations {

    @Get
    override fun getExtras(): List<Extra> = extraService.getExtras()
}
