package hu.microsec.microsla.extras.service

import hu.microsec.microsla.extras.domain.Extra
import hu.microsec.microsla.extras.repository.ExtraRepository
import javax.inject.Singleton

@Singleton
class ExtraService(private val extraRepository: ExtraRepository) {
    fun getExtras(): List<Extra> = extraRepository.findAll()
}
