package hu.microsec.microsla.extras

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.micronaut.test.annotation.MicronautTest

@MicronautTest
class MicroslaExtrasTest(
        private val extraClient: ExtraClient
) : StringSpec({

    "get all extras" {
        val extras = extraClient.getExtras()
        extras.size.shouldBe(7)
        extras.first().id.shouldNotBe(null)
    }
})
