package hu.microsec.microsla.gateway

import hu.microsec.microsla.common.Extra
import hu.microsec.microsla.common.ExtraPrice
import hu.microsec.microsla.gateway.controller.GatewayOperations
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Consumes
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.annotation.Client
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule

@Client("/api/v1/gateway")
interface GatewayClient : GatewayOperations {
    @Get("/extras")
    override fun getExtras(): List<Extra>

    @Get("/price/base")
    @Secured(SecurityRule.IS_ANONYMOUS)
    override fun getBasePrice(): Int

    @Get("/price/extras")
    @Secured(SecurityRule.IS_ANONYMOUS)
    override fun getExtraPrices(): List<ExtraPrice>

    @Get("/price/config")
    @Secured(SecurityRule.IS_ANONYMOUS)
    override fun getPriceForConfiguration(@QueryValue extraIds: List<String>): Int

    @Get("/image")
    @Consumes(MediaType.IMAGE_PNG)
    override fun getImage(@QueryValue extraIds: List<String>): ByteArray

    @Get("/pdf")
    override fun generatePdf(@QueryValue extraIds: List<String>): ByteArray
}
