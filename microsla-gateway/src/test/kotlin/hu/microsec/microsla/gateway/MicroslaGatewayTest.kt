package hu.microsec.microsla.gateway

import hu.microsec.microsla.common.Extra
import hu.microsec.microsla.common.ExtraPrice
import hu.microsec.microsla.gateway.client.ExtraClient
import hu.microsec.microsla.gateway.client.ImageClient
import hu.microsec.microsla.gateway.client.PdfClient
import hu.microsec.microsla.gateway.client.PricingClient
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import io.mockk.every
import io.mockk.mockk

private const val BASE_PRICE = 1000
private const val EXTRA_ID = "extraId"
private val EXTRA_IDS = listOf("first", "second")
private val PRICES = listOf(ExtraPrice(EXTRA_ID, 10))
private val CONFIGURATION = listOf("firstExtra", "secondExtra")
private const val PRICE_FOR_CONFIGURATION = 2000

@MicronautTest
class MicroslaGatewayTest(
        private val gatewayClient: GatewayClient
) : StringSpec({

    "get extras" {
        val extras = gatewayClient.getExtras()
        extras.size.shouldBe(2)
    }

    "get base price" {
        gatewayClient.getBasePrice().shouldBe(BASE_PRICE)
    }

    "get extra prices" {
        val extraPrices = gatewayClient.getExtraPrices()
        extraPrices.size.shouldBe(1)
        extraPrices.first().id.shouldBe(EXTRA_ID)
    }

    "get price for configuration" {
        gatewayClient.getPriceForConfiguration(CONFIGURATION).shouldBe(PRICE_FOR_CONFIGURATION)
    }

    "get image" {
        gatewayClient.getImage(EXTRA_IDS).should {
            it.size.shouldBe(1)
            it[0].shouldBe(1.toByte())
        }
    }

    "get pdf" {
        gatewayClient.getImage(EXTRA_IDS).should {
            it.size.shouldBe(1)
            it[0].shouldBe(1.toByte())
        }
    }
}) {
    @MockBean(ExtraClient::class)
    fun exrasClient(): ExtraClient {
        val mock = mockk<ExtraClient>()
        every { mock.getExtras() } returns listOf(
                Extra("first", "First"),
                Extra("second", "Second")
        )
        return mock
    }

    @MockBean(PricingClient::class)
    fun pricingClient(): PricingClient {
        val mock = mockk<PricingClient>()
        every { mock.getBasePrice() } returns BASE_PRICE
        every { mock.getExtraPrices() } returns PRICES
        every { mock.getPriceForConfiguration(CONFIGURATION) } returns PRICE_FOR_CONFIGURATION
        return mock
    }

    @MockBean(ImageClient::class)
    fun imageClient(): ImageClient {
        val mock = mockk<ImageClient>()
        every { mock.getImage(EXTRA_IDS) } returns ByteArray(1) { 1 }
        return mock
    }

    @MockBean(PdfClient::class)
    fun pdfClient(): PdfClient {
        val mock = mockk<PdfClient>()
        every { mock.generatePdf(EXTRA_IDS) } returns ByteArray(1) { 1 }
        return mock
    }
}
