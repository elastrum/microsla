package hu.microsec.microsla.gateway.client

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.nulls.shouldNotBeNull

class ImageFallbackClientTest : StringSpec({

    "fallback image" {
        ImageFallbackClient().getImage(listOf()).shouldNotBeNull()
    }
})
