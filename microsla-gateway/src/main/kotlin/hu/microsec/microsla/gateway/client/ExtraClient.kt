package hu.microsec.microsla.gateway.client

import hu.microsec.microsla.common.Extra
import hu.microsec.microsla.common.ExtraOperations
import io.micronaut.http.annotation.Get
import io.micronaut.http.client.annotation.Client

@Client(id = "microsla-extras")
interface ExtraClient : ExtraOperations {

    @Get("/api/v1/extras")
    override fun getExtras(): List<Extra>
}
