package hu.microsec.microsla.gateway.controller

import hu.microsec.microsla.common.ExtraOperations
import hu.microsec.microsla.common.ImageOperations
import hu.microsec.microsla.common.PdfOperations
import hu.microsec.microsla.common.PricingOperations

interface GatewayOperations : ExtraOperations, PricingOperations, ImageOperations, PdfOperations
