package hu.microsec.microsla.gateway.client

import hu.microsec.microsla.common.ImageOperations
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Consumes
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.annotation.Client
import io.micronaut.retry.annotation.Recoverable

@Recoverable(api = ImageOperations::class)
@Client(id = "microsla-image", path = "/api/v1/image")
interface ImageClient : ImageOperations {
    @Get
    @Consumes(MediaType.IMAGE_PNG)
    override fun getImage(@QueryValue extraIds: List<String>): ByteArray
}
