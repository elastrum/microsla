package hu.microsec.microsla.gateway.client

import hu.microsec.microsla.common.ExtraPrice
import hu.microsec.microsla.common.PricingOperations
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.annotation.Client

@Client(id = "microsla-pricing", path = "/api/v1/pricing")
interface PricingClient : PricingOperations {

    @Get("/base")
    override fun getBasePrice(): Int

    @Get("/config")
    override fun getPriceForConfiguration(@QueryValue extraIds: List<String>): Int

    @Get("/extras")
    override fun getExtraPrices(): List<ExtraPrice>
}
