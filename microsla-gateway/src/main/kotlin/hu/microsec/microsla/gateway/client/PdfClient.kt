package hu.microsec.microsla.gateway.client

import hu.microsec.microsla.common.APPLICATION_PDF
import hu.microsec.microsla.common.PdfOperations
import io.micronaut.http.annotation.Consumes
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.annotation.Client

@Client("microsla-pdf", path = "/api/v1/pdf")
interface PdfClient : PdfOperations {
    @Get
    @Consumes(APPLICATION_PDF)
    override fun generatePdf(@QueryValue extraIds: List<String>): ByteArray
}
