package hu.microsec.microsla.gateway.client

import hu.microsec.microsla.common.ImageOperations
import io.micronaut.http.annotation.QueryValue
import io.micronaut.retry.annotation.CircuitBreaker
import io.micronaut.retry.annotation.Fallback

@Fallback
@CircuitBreaker(attempts = "5", reset = "10s")
class ImageFallbackClient : ImageOperations {
    override fun getImage(@QueryValue extraIds: List<String>): ByteArray =
            ImageFallbackClient::class.java.classLoader.getResource("fallback.png")!!.readBytes()
}
