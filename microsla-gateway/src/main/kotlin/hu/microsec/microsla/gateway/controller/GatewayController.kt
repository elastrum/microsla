package hu.microsec.microsla.gateway.controller

import hu.microsec.microsla.common.APPLICATION_PDF
import hu.microsec.microsla.common.Extra
import hu.microsec.microsla.common.ExtraPrice
import hu.microsec.microsla.gateway.client.ExtraClient
import hu.microsec.microsla.gateway.client.ImageClient
import hu.microsec.microsla.gateway.client.PdfClient
import hu.microsec.microsla.gateway.client.PricingClient
import io.micronaut.http.HttpHeaders.CACHE_CONTROL
import io.micronaut.http.HttpHeaders.CONTENT_DISPOSITION
import io.micronaut.http.HttpHeaders.EXPIRES
import io.micronaut.http.HttpHeaders.PRAGMA
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.QueryValue
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule

private const val PDF_FILENAME = "your_model_e.pdf"
private const val PNG_FILENAME = "your_model_e.png"

@Controller("api/v1/gateway")
@Secured(SecurityRule.IS_ANONYMOUS)
class GatewayController(
        private val extraClient: ExtraClient,
        private val pricingClient: PricingClient,
        private val imageClient: ImageClient,
        private val pdfClient: PdfClient
) : GatewayOperations {

    @Get("/extras")
    override fun getExtras(): List<Extra> = extraClient.getExtras()

    @Get("/price/base")
    override fun getBasePrice(): Int = pricingClient.getBasePrice()

    @Get("/price/extras")
    override fun getExtraPrices(): List<ExtraPrice> = pricingClient.getExtraPrices()

    @Get("/price/config")
    override fun getPriceForConfiguration(@QueryValue(defaultValue = "") extraIds: List<String>): Int = pricingClient.getPriceForConfiguration(extraIds)

    @Get("/image")
    @Produces(MediaType.IMAGE_PNG)
    fun getImageWithHeaders(@QueryValue(defaultValue = "") extraIds: List<String>): HttpResponse<ByteArray> =
            HttpResponse.ok(getImage(extraIds)).headers {
                it[CONTENT_DISPOSITION] = "attachment; filename=\"$PNG_FILENAME\""
                it[CACHE_CONTROL] = "no-cache, no-store, must-revalidate"
                it[PRAGMA] = "no-cache"
                it[EXPIRES] = "0"
            }

    override fun getImage(extraIds: List<String>): ByteArray = imageClient.getImage(extraIds)

    @Get("/pdf")
    @Produces(APPLICATION_PDF)
    fun generatePdfWithHeaders(@QueryValue(defaultValue = "") extraIds: List<String>): HttpResponse<ByteArray> =
            HttpResponse.ok(generatePdf(extraIds)).headers {
                it[CONTENT_DISPOSITION] = "inline; filename=\"$PDF_FILENAME\""
                it[CACHE_CONTROL] = "no-cache, no-store, must-revalidate"
                it[PRAGMA] = "no-cache"
                it[EXPIRES] = "0"
            }

    override fun generatePdf(extraIds: List<String>): ByteArray = pdfClient.generatePdf(extraIds)
}
