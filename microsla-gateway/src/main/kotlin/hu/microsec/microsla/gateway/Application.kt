package hu.microsec.microsla.gateway

import io.micronaut.runtime.Micronaut.*
import io.swagger.v3.oas.annotations.*
import io.swagger.v3.oas.annotations.info.*

@OpenAPIDefinition(
    info = Info(
            title = "Microsla API",
            version = "0.0.1",
			contact = Contact(url = "https://www.microsec.hu", name = "Ákos Dohovits", email = "akos.dohovits@microsec.hu")
    )
)
object Api

fun main(args: Array<String>) {
	build()
	    .args(*args)
		.packages("hu.microsec.microsla.gateway")
		.start()
}

