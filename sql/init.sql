CREATE DATABASE `microsla-extras`;
CREATE DATABASE `microsla-image`;
CREATE DATABASE `microsla-pricing`;

CREATE USER 'microsla-extras'@'%' IDENTIFIED WITH mysql_native_password BY 'microsla-extras';
CREATE USER 'microsla-image'@'%' IDENTIFIED WITH mysql_native_password BY 'microsla-image';
CREATE USER 'microsla-pricing'@'%' IDENTIFIED WITH mysql_native_password BY 'microsla-pricing';

GRANT ALL PRIVILEGES ON `microsla-extras`.* TO 'microsla-extras';
GRANT ALL PRIVILEGES ON `microsla-image`.* TO 'microsla-image';
GRANT ALL PRIVILEGES ON `microsla-pricing`.* TO 'microsla-pricing';
