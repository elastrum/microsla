package hu.microsec.microsla.pdf.controller

import hu.microsec.microsla.common.APPLICATION_PDF
import hu.microsec.microsla.common.PdfOperations
import hu.microsec.microsla.pdf.service.PdfService
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.QueryValue

@Controller("/api/v1/pdf")
class PdfController(private val pdfService: PdfService) : PdfOperations {

    @Get
    @Produces(APPLICATION_PDF)
    override fun generatePdf(@QueryValue extraIds: List<String>): ByteArray = pdfService.generatePdf(extraIds)
}
