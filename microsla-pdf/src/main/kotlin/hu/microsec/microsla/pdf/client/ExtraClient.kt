package hu.microsec.microsla.pdf.client

import hu.microsec.microsla.common.Extra
import hu.microsec.microsla.common.ExtraOperations
import io.micronaut.http.annotation.Get
import io.micronaut.http.client.annotation.Client

@Client("microsla-extras", path = "/api/v1/extras")
interface ExtraClient : ExtraOperations {

    @Get
    override fun getExtras(): List<Extra>
}
