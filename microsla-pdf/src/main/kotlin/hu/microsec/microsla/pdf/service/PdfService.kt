package hu.microsec.microsla.pdf.service

import com.github.timrs2998.pdfbuilder.*
import com.github.timrs2998.pdfbuilder.style.Alignment
import com.github.timrs2998.pdfbuilder.style.Margin
import com.github.timrs2998.pdfbuilder.style.Padding
import hu.microsec.microsla.pdf.client.ExtraClient
import hu.microsec.microsla.pdf.client.ImageClient
import hu.microsec.microsla.pdf.client.PricingClient
import org.apache.pdfbox.pdmodel.font.PDType1Font
import java.awt.Color
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO
import javax.inject.Singleton

@Singleton
class PdfService(
        private val imageClient: ImageClient,
        private val pricingClient: PricingClient,
        private val extraClient: ExtraClient
) {
    fun generatePdf(extraIds: List<String>): ByteArray = ByteArrayOutputStream()
            .also(document { buildDocument(extraIds) }::save)
            .toByteArray()

    private fun Document.buildDocument(extraIds: List<String>) {
        val marginSize = 15f
        margin = Margin(marginSize, marginSize, marginSize, marginSize)
        text("Your Model E") {
            fontSize = 18f
            horizontalAlignment = Alignment.CENTER
        }
        image(ImageIO.read(ByteArrayInputStream(imageClient.getImage(extraIds)))) {
            imgHeight = 300
            imgWidth = 300
            horizontalAlignment = Alignment.CENTER
            val paddingSize = 10f
            padding = Padding(paddingSize, paddingSize, paddingSize, paddingSize)
        }

        val totalPrice = pricingClient.getPriceForConfiguration(extraIds)
        text("Your price: $totalPrice €") {
            pdFont = PDType1Font.TIMES_BOLD
            fontSize = 14f
            horizontalAlignment = Alignment.LEFT
            val paddingSize = 10f
            padding = Padding(paddingSize, 0f, paddingSize, 0f)
        }

        text("Selected options: ") {
            fontSize = 12f
            horizontalAlignment = Alignment.LEFT
            val paddingSize = 5f
            padding = Padding(paddingSize, 0f, paddingSize, 0f)
        }

        val extras = extraClient.getExtras().filter { extraIds.contains(it.id) }
        val prices = pricingClient.getExtraPrices()
        val extrasWithPrice = extras.map {
            it.description to (prices.find { price -> price.id == it.id }?.price)
        }.toMap()
        table {

            header {
                backgroundColor = Color.YELLOW
                pdFont = PDType1Font.TIMES_BOLD

                text("Option") {
                    padding = Padding(0f, 0f, 6f, 0f)
                }
                text("Price")
            }

            extrasWithPrice.forEach {
                row {
                    text(it.key)
                    text(it.value?.toString()?.plus(" €") ?: "N/A")
                }
            }
        }
    }
}
