package hu.microsec.microsla.pdf.client

import hu.microsec.microsla.common.ExtraPrice
import hu.microsec.microsla.common.PricingOperations
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.annotation.Client

@Client("microsla-pricing", path = "/api/v1/pricing")
interface PricingClient : PricingOperations {
    @Get("/extras")
    override fun getExtraPrices(): List<ExtraPrice>

    @Get("/config")
    override fun getPriceForConfiguration(@QueryValue extraIds: List<String>): Int
}
