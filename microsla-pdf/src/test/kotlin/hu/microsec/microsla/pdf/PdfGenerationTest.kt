package hu.microsec.microsla.pdf

import hu.microsec.microsla.common.Extra
import hu.microsec.microsla.common.ExtraPrice
import hu.microsec.microsla.pdf.client.ExtraClient
import hu.microsec.microsla.pdf.client.ImageClient
import hu.microsec.microsla.pdf.client.PricingClient
import hu.microsec.microsla.pdf.service.PdfService
import io.kotest.core.spec.style.StringSpec
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import io.mockk.every
import io.mockk.mockk
import java.io.FileOutputStream

private const val BASE_PRICE = 1000
private val EXTRA_IDS = listOf("first", "second")
private val PRICES = EXTRA_IDS.mapIndexed { index, it -> ExtraPrice(it, index * 100) }

@MicronautTest
class PdfGenerationTest(private val pdfService: PdfService) : StringSpec({

    "generate PDF" {
        val pdf = pdfService.generatePdf(listOf(
                "first",
                "second"
        ))

        FileOutputStream("build/output.pdf").use {
            it.write(pdf)
        }
    }
}) {
    @MockBean(ExtraClient::class)
    fun exrasClient(): ExtraClient {
        val mock = mockk<ExtraClient>()
        every { mock.getExtras() } returns listOf(
                Extra("first", "First"),
                Extra("second", "Second")
        )
        return mock
    }

    @MockBean(PricingClient::class)
    fun pricingClient(): PricingClient {
        val mock = mockk<PricingClient>()
        every { mock.getBasePrice() } returns BASE_PRICE
        every { mock.getExtraPrices() } answers { PRICES }
        every { mock.getPriceForConfiguration(EXTRA_IDS) } returns 1300
        return mock
    }

    @MockBean(ImageClient::class)
    fun imageClient(): ImageClient {
        val mock = mockk<ImageClient>()
        every { mock.getImage(EXTRA_IDS) } returns
                PdfGenerationTest::class.java.classLoader.getResource("placeholder.png")!!.readBytes()
        return mock
    }
}
