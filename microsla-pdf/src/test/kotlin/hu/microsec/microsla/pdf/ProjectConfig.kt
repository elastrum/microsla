package hu.microsec.microsla.pdf

import io.kotest.core.config.AbstractProjectConfig
import io.micronaut.test.extensions.kotest.MicronautKotestExtension

@Suppress("unused") // this config is automatically read by kotest
object ProjectConfig : AbstractProjectConfig() {
    override fun listeners(): List<MicronautKotestExtension> = listOf(MicronautKotestExtension)
    override fun extensions(): List<MicronautKotestExtension> = listOf(MicronautKotestExtension)
}
