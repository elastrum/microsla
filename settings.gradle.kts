rootProject.name = "microsla"
include(
"microsla-common",
"microsla-gateway",
"microsla-extras",
"microsla-image",
"microsla-pdf",
"microsla-pricing"
)
