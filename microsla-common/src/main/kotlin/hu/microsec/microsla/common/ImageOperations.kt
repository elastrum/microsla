package hu.microsec.microsla.common

interface ImageOperations {
    fun getImage(extraIds: List<String>): ByteArray
}
