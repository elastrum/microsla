package hu.microsec.microsla.common

open class ExtraPrice(open val id: String, open val price: Int) {
    @Suppress("unused")
    constructor() : this("", 0)
}
