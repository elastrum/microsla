package hu.microsec.microsla.common

interface PricingOperations {
    fun getBasePrice(): Int
    fun getExtraPrices(): List<ExtraPrice>
    fun getPriceForConfiguration(extraIds: List<String>): Int
}
