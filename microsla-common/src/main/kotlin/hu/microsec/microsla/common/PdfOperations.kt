package hu.microsec.microsla.common

interface PdfOperations {
    fun generatePdf(extraIds: List<String>): ByteArray
}
