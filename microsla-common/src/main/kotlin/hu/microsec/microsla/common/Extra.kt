package hu.microsec.microsla.common

open class Extra(open val id: String, open val description: String) {
    @Suppress("unused")
    constructor() : this("", "")
}
