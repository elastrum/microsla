package hu.microsec.microsla.common

interface ExtraOperations {
    fun getExtras(): List<Extra>
}
